const baseUrl = 'http://localhost:3000/api/game-room/result'
const roomCode = document.querySelector('#room-code').innerText
const boxPlayerOne = document.querySelectorAll('.box-one')
const boxPlayerTwo = document.querySelectorAll('.box-two')
const textResult = document.querySelector('.text-result')
const textVs = document.querySelector('.text-vs')
const textError = document.querySelector('.text-error')

let state = 0

if (!textResult.textContent) state = 1

const sendChoice = (playerOnePicked, playerTwoPicked) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')

  const body = JSON.stringify({ roomCode, playerOnePicked, playerTwoPicked })

  const requestOptions = {
    method: 'POST',
    headers,
    body,
    redirect: 'follow',
  }

  fetch(baseUrl, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)

      if (result.errors) {
        textError.textContent = 'oopss.. something wrong'
        textError.classList.remove('d-none')
        textVs.classList.add('d-none')
      }

      if (result.success) {
        textResult.textContent = result.result
        textVs.classList.add('d-none')
      }
    })
    .catch((error) => console.log('error', error))
}

const changeState = (box, playerOnePicked, playerTwoPicked) => {
  if (state) {
    box.classList.add('grey-box')
    sendChoice(playerOnePicked, playerTwoPicked)
    state = 0
    console.log('memilih')
  }
}

const playerOneClick = (index, choice) => {
  changeState(boxPlayerOne[index], choice, '')
}

const playerTwoClick = (index, choice) => {
  changeState(boxPlayerTwo[index], '', choice)
}
