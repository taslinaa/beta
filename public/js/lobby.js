// Create Room
const createRoomUrl = 'http://localhost:3000/api/game-room/create'

const createBtn = document.querySelector('#create')
const joinBtn = document.querySelector('#join')
const alertFailed = document.querySelector('.alert-danger')
const alertSuccess = document.querySelector('.alert-success')
const msgFailed = document.querySelector('#msg-failed')
const msgSuccess = document.querySelector('#msg-success')

const createRoom = (url, playerOne) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')

  const body = JSON.stringify({ playerOne })

  const requestOptions = {
    method: 'POST',
    headers,
    body,
    redirect: 'follow',
  }

  fetch(url, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)
      const { roomCode } = result.room

      if (result.errors) {
        msgFailed.textContent = result.errors
        alertFailed.classList.add('show')
      }

      if (result.success) {
        msgSuccess.textContent = result.success
        alertSuccess.classList.add('show')
        setInterval(() => {
          location.pathname = `/game/play/${roomCode}`
        }, 2000);
      }
    })
    .catch((error) => console.log('error', error))
}

createBtn.addEventListener('click', (ev) => {
  ev.preventDefault()
  const playerOne = ev.target.value
  createRoom(createRoomUrl, playerOne)
})

// Join Room
const joinRoomUrl = 'http://localhost:3000/api/game-room/join'

const joinRoom = (url, playerTwo, roomCode) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')

  const body = JSON.stringify({ playerTwo, roomCode })

  const requestOptions = {
    method: 'PATCH',
    headers,
    body,
    redirect: 'follow',
  }

  fetch(url, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)

      if (result.errors) {
        msgFailed.textContent = result.errors
        alertFailed.classList.add('show')
      }

      if (result.success) {
        msgSuccess.textContent = result.success
        alertSuccess.classList.add('show')
        setInterval(() => {
          location.pathname = `/game/play/${roomCode}`
        }, 2000);
      }
    })
    .catch((error) => console.log('error', error));
}

joinBtn.addEventListener('click', (ev) => {
  ev.preventDefault()
  const playerTwo = ev.target.value
  const roomCode = document.querySelector('#room-code').value
  console.log(playerTwo, roomCode)
  joinRoom(joinRoomUrl, playerTwo, roomCode)
})
