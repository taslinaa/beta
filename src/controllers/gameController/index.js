import {
  GameRoom, History,
} from '../../models'

export default class GameController {
  static generateRoomCode = () => {
    let roomCode = 'RPS'
    let i = 0
    const numbers = '0123456789'
    const numLength = numbers.length
    do {
      roomCode += numbers.charAt(Math.floor(Math.random() * numLength))
      i += 1
    } while (i < 7)

    return roomCode
  }

  static generateResult = (playerOnePicked, playerTwoPicked) => {
    let result = ''

    if (playerOnePicked === playerTwoPicked) result = 'draw'
    if (playerOnePicked === 'rock' && playerTwoPicked === 'scissors') result = 'player one win'
    if (playerOnePicked === 'paper' && playerTwoPicked === 'rock') result = 'player one win'
    if (playerOnePicked === 'scissors' && playerTwoPicked === 'paper') result = 'player one win'

    if (playerTwoPicked === playerOnePicked) result = 'draw'
    if (playerTwoPicked === 'rock' && playerOnePicked === 'scissors') result = 'player two win'
    if (playerTwoPicked === 'paper' && playerOnePicked === 'rock') result = 'player two win'
    if (playerTwoPicked === 'scissors' && playerOnePicked === 'paper') result = 'player two win'

    return result
  }

  static createRoom = async (req, res) => {
    const { playerOne } = req.body
    const roomCode = this.generateRoomCode()

    try {
      const room = await GameRoom.create({ roomCode, playerOne })

      if (!room) {
        return res.status(422).json({ errors: 'something went wrong' })
      }

      return res.status(200).json({ success: `success create room. Room Code : ${room.roomCode}`, room })
    } catch (error) {
      return res.status(400).json({ errors: error.message })
    }
  }

  static joinRoom = async (req, res) => {
    const { roomCode, playerTwo } = req.body

    try {
      const oriRoom = await GameRoom.findOne({ where: { roomCode } })
      if (!oriRoom) {
        return res.status(404).json({ errors: 'failed', message: 'room not found' })
      }

      const isRoomFull = (oriRoom.playerTwo) ? 1 : 0
      if (isRoomFull) {
        return res.status(409).json({ errors: 'failed join room', message: 'room is full' })
      }

      const updRoom = await GameRoom.update(
        { playerTwo },
        { where: { roomCode }, returning: true },
      )

      return res.status(200).json({ success: 'success join room', room: updRoom[1][0] })
    } catch (error) {
      return res.status(500).json({ errors: error.message })
    }
  }

  static result = async (req, res) => {
    let result = ''

    const { roomCode, playerOnePicked = '', playerTwoPicked = '' } = req.body

    try {
      const room = await GameRoom.findOne({ where: { roomCode } })
      if (!room) return res.status(404).json({ errors: 'failed', message: 'room doesnt exists' })

      if (playerOnePicked) {
        room.set('playerOnePicked', playerOnePicked)
        room.save()
        result = 'waiting player two'
      }
      if (playerTwoPicked) {
        room.set('playerTwoPicked', playerTwoPicked)
        room.save()
        result = 'waiting player one'
      }

      if (room.playerOnePicked && room.playerTwoPicked) {
        result = this.generateResult(room.playerOnePicked, room.playerTwoPicked)
        room.set('result', result)
        room.save()

        const { playerOne, playerTwo } = room
        await History.create(
          {
            playerOne,
            playerTwo,
            playerOnePicked: room.playerOnePicked,
            playerTwoPicked: room.playerTwoPicked,
            result,
          },
        )
      }

      return res.status(200).json({ success: 'success', result })
    } catch (error) {
      console.log(error)
      return res.status(400).json({ errors: error.message })
    }
  }
}
