import {
  UserGame, UserBiodata, UserHistory, History, sequelize,
} from '../../models'
import { JwtAuth, validateHandler } from '../../utils'

export default class UserController {
  static OPTIONS = {
    include: [{
      model: UserBiodata,
      as: 'biodata',
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
    }, {
      model: UserHistory,
      reequired: true,
      as: 'history',
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
    }],
    attributes: {
      exclude: ['createdAt', 'updatedAt', 'password'],
    },
    distinct: true,
    col: UserGame.id,
  }

  static setCookies = (res, token) => {
    res.cookie('jwt', token, {
      httpOnly: true,
      maxAge: 24 * 60 * 60 * 1000,
    })
  }

  static register = async (req, res) => {
    const { username, password } = req.body
    const transaction = await sequelize.transaction()

    try {
      const newUser = await UserGame.create({ username, password }, { transaction })

      if (newUser) {
        const { email, gender } = req.body
        const newBio = await UserBiodata.create(
          { email, gender, userId: newUser.id },
          { transaction },
        )

        await newUser.setBiodata(newBio, { transaction })

        await transaction.commit()
      }

      return res.status(201).json({
        success: 'success to registered. please login',
        user: newUser.username,
      })
    } catch (error) {
      console.log(error.message)
      await transaction.rollback()
      const err = error.errors[0].message
      return res.status(409).json({ errors: err })
    }
  }

  static login = async (req, res) => {
    const { username, password } = req.body

    try {
      const user = await UserGame.login(username, password)
      const payload = { id: user.id, username: user.username }

      return JwtAuth.sign(
        payload,
        (token) => {
          this.setCookies(res, token)

          return res.status(200)
            .json({ success: 'success login', user: user.username, token })
        },
      )
    } catch (error) {
      const errors = validateHandler(error)
      return res.status(401).json({ errors })
    }
  }

  static findAll = async (req, res) => {
    const limit = 5
    const { page = 1 } = req.query
    const query = { where: { role: 'user' } }

    try {
      const users = await UserGame.findAndCountAll({
        ...query,
        ...this.OPTIONS,
        limit,
        offset: (page - 1) * limit,
      })
      const maxPage = Math.ceil(users.count / 5)
      const incomingPage = parseInt(page, 10) + 1

      return res.status(200).json({ users: users.rows, maxPage, incomingPage })
    } catch (error) {
      return res.status(500).json({ errors: error.message })
    }
  }

  static findOne = (req, res) => {
    const { username } = req.params

    return UserGame.findOne({
      where: { username },
      ...this.OPTIONS,
    })
      .then((user) => {
        if (!user) return res.status(404).json({ errors: 'user not found' })

        return res.status(200).json(user)
      })
      .catch((err) => res.status(400).json({ errors: err.message }))
  }

  static logout = (req, res) => {
    res.cookie('jwt', '', {
      maxAge: 1,
    })
    res.redirect('/')
  }

  static update = async (req, res) => {
    const {
      about, email, username, phone, gender,
    } = req.body
    const transaction = await sequelize.transaction()

    try {
      const updUser = await UserGame.update({ username },
        { where: { username: req.params.username }, transaction, returning: ['id', 'username'] })

      if (!updUser[0]) return res.status(404).json({ errors: 'fialed update', message: 'user not found' })

      const { id, username: uname } = updUser[1][0]
      const payload = { id, user: uname }

      await UserBiodata.update(
        {
          email, about, phone, gender,
        },
        {
          where: { userId: id },
          transaction,
        },
      )

      await transaction.commit()

      return JwtAuth.sign(
        payload,
        (token) => {
          this.setCookies(res, token)

          return res.status(200)
            .json({ success: 'success update', user: uname, token })
        },
      )
    } catch (error) {
      await transaction.rollback()
      return res.status(409).json({ errors: error.message })
    }
  }

  static delete = async (req, res) => {
    const { username } = req.params

    try {
      const isDeleted = await UserGame.destroy({
        where: { username },
      })

      if (!isDeleted) return res.status(404).json({ errors: 'failed to delete. User not found' })

      return res.status(200).json({ success: 'success deleted user' })
    } catch (error) {
      return res.status(500).json({ errors: error.message })
    }
  }

  static findHistory = async (req, res) => {
    const limit = 5
    const { page = 1 } = req.query

    try {
      const histories = await History.findAndCountAll({
        limit,
        offset: (page - 1) * limit,
      })

      const maxPage = Math.ceil(histories.count / 5)
      const incomingPage = parseInt(page, 10) + 1

      return res.status(200).json({ histories: histories.rows, maxPage, incomingPage })
    } catch (error) {
      return res.status(500).json({ errors: error.message })
    }
  }
}
