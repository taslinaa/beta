module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('History', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      playerOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      playerTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      playerOnePicked: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
        allowNull: false,
      },
      playerTwoPicked: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
        allowNull: false,
      },
      result: {
        type: Sequelize.ENUM,
        values: ['player one won', 'player two won', 'draw'],
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('History')
  },
}
