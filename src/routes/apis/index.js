import { Router } from 'express'
import UserController from '../../controllers/apiController'
import GameController from '../../controllers/gameController'
import UserValidator from '../../validators/UserValidator'
import Middlewares from '../../middlewares'

const router = Router()

router.get('/users', Middlewares.Admin, UserController.findAll)
router.get('/user/:username/profile', UserController.findOne)
router.patch('/user/:username', [UserValidator.update, Middlewares.Auth], UserController.update)
router.delete('/user/:username', [Middlewares.Auth], UserController.delete)
router.post('/user/login', [UserValidator.login, Middlewares.Guest], UserController.login)
router.post('/user/register', [UserValidator.register, Middlewares.Guest], UserController.register)
router.get('/user/logout', UserController.logout)
router.get('/users/history', Middlewares.Admin, UserController.findHistory)

router.post('/game-room/create', Middlewares.Auth, GameController.createRoom)
router.patch('/game-room/join', Middlewares.Auth, GameController.joinRoom)
router.post('/game-room/result', Middlewares.Auth, GameController.result)

export default router
