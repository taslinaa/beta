import { validationResult } from 'express-validator'
import { AuthMiddleware, AuthAdmin } from './Auth'

export default class Middleware {
  static Guest = (req, res, next) => this.handler('guest', req, res, next)

  static Auth = (req, res, next) => this.handler('auth', req, res, next)

  static Admin = (req, res, next) => this.handler('admin', req, res, next)

  static handler = (type, req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      const message = errors.array()[0].msg
      console.log(message)
      return res.status(400).json({ errors: message })
    }

    switch (type) {
      case 'guest': return next()
      case 'auth': return AuthMiddleware(req, res, next)
      case 'admin': return AuthAdmin(req, res, next)
      default: return res.status(403)
    }
  }
}
