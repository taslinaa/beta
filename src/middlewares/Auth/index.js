import { UserGame, UserBiodata } from '../../models'
import { JwtAuth } from '../../utils'

const AuthMiddleware = (req, res, next) => {
  const token = req.cookies.jwt

  if (token) {
    return JwtAuth.verify(
      token,
      ((decoded) => {
        console.log(decoded)
        next()
      }),
      ((err) => res.status(500).json({ error: err.message })),
    )
  }

  return res.redirect('/user/login')
}

const AuthAdmin = (req, res, next) => {
  const token = req.cookies.jwt

  if (token) {
    return JwtAuth.verify(
      token,
      (async (decoded) => {
        const admin = await UserGame.findByPk(decoded.id)
        if (admin && admin.role === 'admin') return next()

        return res.status(403).render('403')
      }),
      ((error) => {
        console.log('AuthAdmin error : ', error.message)
      }),
    )
  }

  return res.status(403).render('403')
}

const checkUser = (req, res, next) => {
  const token = req.cookies.jwt
  if (token) {
    return JwtAuth.verify(
      token,
      (async (decoded) => {
        const user = await UserGame.findByPk(decoded.id, {
          include: [{ model: UserBiodata, as: 'biodata' }],
        })
        res.locals.user = user
        next()
      }),
      ((error) => {
        console.log('checkuser error : ', error.message)
        res.locals.user = null
        next()
      }),
    )
  }

  res.locals.user = null
  return next()
}

export {
  AuthMiddleware,
  checkUser,
  AuthAdmin,
}
