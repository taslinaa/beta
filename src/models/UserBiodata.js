const { Model } = require('sequelize')
const { ENUMS } = require('../utils/ENUM')

module.exports = (sequelize, DataTypes) => {
  class UserBiodata extends Model {
    static associate(models) {
      const { UserGame } = models

      UserBiodata.belongsTo(UserGame, {
        foreignKey: 'userId',
        as: 'user',
      })
    }
  }
  UserBiodata.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      about: {
        type: DataTypes.STRING,
        defaultValue: '',
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          msg: 'email has been registered',
        },
        validate: {
          isEmail: {
            msg: 'please insert a valid email example : foo@email.com',
          },
          notEmpty: {
            msg: 'please insert an email',
          },
        },
      },
      phone: {
        type: DataTypes.STRING,
        defaultValue: '',
      },
      gender: {
        type: DataTypes.ENUM,
        values: ENUMS.gender,
      },
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'UserBiodata',
    },
  )
  return UserBiodata
}
