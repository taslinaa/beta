const { UserGame } = require('../models')
const { dataUser } = require('../dataseeders')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await UserGame.bulkCreate(dataUser, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserGame', null, {})
  },
}
