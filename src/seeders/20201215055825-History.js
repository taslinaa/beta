const { history } = require('../dataseeders')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('History', history, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('History', null, {})
  },
}
