const { genSalt, hash } = require('bcrypt')
const jwt = require('jsonwebtoken')

const hashing = async (user) => {
  const formatUser = user
  console.log('before hash', formatUser.password)

  const salt = await genSalt(10)
  formatUser.password = await hash(formatUser.password, salt)
  console.log('after has: ', formatUser.password)

  return formatUser
}

class JwtAuth {
  static opt = {
    algorithm: 'HS256',
    expiresIn: 24 * 60 * 60,
  }

  static sign = (payload, success, error) => jwt.sign(
    payload,
    process.env.JWT_SECRET,
    this.opt,
    (err, token) => {
      if (err) return error(err)

      return success(token)
    },
  )

  static verify = (token, success, error) => jwt.verify(
    token,
    process.env.JWT_SECRET,
    (err, decoded) => {
      if (err) return error(err)

      return success(decoded)
    },
  )
}

const validateHandler = (err) => {
  const errors = {
    username: '',
    password: '',
  }

  // incorrect username
  if (err.message === 'incorrect username') errors.username = 'username not found'

  if (err.message === 'incorrect password') errors.password = 'invalid password'

  return errors
}

module.exports = {
  hashing,
  JwtAuth,
  validateHandler,
}
